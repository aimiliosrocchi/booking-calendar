export class TokenDto {
  value: String | undefined;

  constructor(value: string){
    this.value = value;
  }
}
