import { TDetailsComponent } from '../t-details/t-details.component';
import { Observable } from "rxjs";
import { BookingService } from "../_services/booking.service";
import { Booking } from "../booking";

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from "../_services/user.service";
@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.css']
})
export class BookingListComponent implements OnInit {

  bookings: Observable<Booking[]> | undefined;
  content: any;

  constructor(private bookingService: BookingService,
              private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getUserBoard().subscribe(
      data =>{
        this.reloadData();
      }, err => {
        if(err.message){
          this.content = err.message;
          alert("You must Login / Register in order to view your bookings");
          this.router.navigate(['/login']);

        }else{
          this.content = JSON.parse(err.error).message;
          alert(this.content);
          this.router.navigate(['/login']);
        }
      }
    );
  }

  reloadData() {
    this.bookings = this.bookingService.getBookingList();
  }

  deleteBooking(id: string){
    this.bookingService.deleteBooking(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  bookingDetails(id: string){
    this.router.navigate(['details', id]);

  }

  updateBooking(id: string){

    this.router.navigate(['update', id]);

  }


}
