export class Event {
  id: number | undefined;
  title: string | undefined;
  endDate: string | undefined;
  startDate: string | undefined;
  isFullDay: boolean | undefined;
}
