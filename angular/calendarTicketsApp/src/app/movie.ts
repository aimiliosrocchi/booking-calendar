export class Movie {
  id: number | undefined;
  title: string | undefined;
  description: string | undefined;
  imageUrl: string | undefined;
  summary: string | undefined;
}
