import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { AppConstants } from '../common/app.constants';
import {FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser} from "angularx-social-login";
import {Router} from "@angular/router";
import {TokenDto} from "../token-dto";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 socialUser: SocialUser | undefined;
 socialUserLogged: SocialUser | undefined;
  form: any = {
    email: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  photo?: string;
  IsSubmitted = false;
  firstName?: string;




  constructor(private authService: AuthService, private tokenStorage: TokenStorageService, private authServiceSocial: SocialAuthService, private router: Router) { }

  ngOnInit(): void {

    this.authServiceSocial.authState.subscribe(
      data => {
        this.socialUserLogged = data;
        this.isLoggedIn = (this.socialUserLogged != null && this.tokenStorage.getToken() != null);
        this.photo = this.socialUserLogged.photoUrl;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.firstName = this.socialUserLogged.firstName;
        this.router.navigate(['/']);

      }
    );

    if (this.socialUserLogged == null) {

      this.isLoggedIn = !!this.tokenStorage.getToken();
      if (this.isLoggedIn) {

        this.tokenStorage.getToken();
        const user = this.tokenStorage.getUser();
        this.roles = user.roles;
        this.firstName = user.firstName;

      }
    }
  }


  signInWithGoogle(): void {
    this.authServiceSocial.signIn(GoogleLoginProvider.PROVIDER_ID).then(data => {
      this.socialUser = data;
      const tokenGoogle = new TokenDto(this.socialUser.idToken);
      console.log(tokenGoogle);
      this.authService.google(tokenGoogle).subscribe(
        res => {
          this.tokenStorage.saveToken(<string>res.value);
          this.isLoggedIn = true;
          this.roles = this.tokenStorage.getUser().roles;
        },
        err =>{
          console.log(err);
          this.errorMessage = err.error;
          this.isLoginFailed = true;

        }
      );
    }).catch(
      err => {
        console.log(err);
      }
    );
  }

  signInWithFB(): void {
    this.authServiceSocial.signIn(FacebookLoginProvider.PROVIDER_ID).then(data => {
      this.socialUser = data;
      const tokenFacebook = new TokenDto(this.socialUser.authToken);
      this.authService.facebook(tokenFacebook).subscribe(
        res => {
          this.tokenStorage.saveToken(<string>res.value);
          this.isLoggedIn = true;
          this.roles = this.tokenStorage.getUser().roles;


        },
        err =>{
          console.log(err);
          this.errorMessage = err.error;
          this.isLoginFailed = true;

        }
      )
    }).catch(
      err => {
        console.log(err);
      }
    );
  }

  onSubmit(): void {
    const { email, password } = this.form;

    this.authService.login(email, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.IsSubmitted = true;
        this.reloadPage();

        },
      err => {
        this.errorMessage = err.error.error + ", status" + ": "+ err.error.status;
        this.isLoginFailed = true;  console.log(err.error.error);

      }

  );

  }

  reloadPage(): void {
    window.location.reload();
  }
}
