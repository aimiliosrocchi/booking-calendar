import {Component, OnInit} from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';
import {SocialAuthService, SocialUser} from "angularx-social-login";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent  implements OnInit{
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  firstName?: string;
  title = 'Calendar Tickets';

  socialUserLogged: SocialUser | undefined;
  photo?: string;
  constructor(private tokenStorageService: TokenStorageService, private authSocialService: SocialAuthService) {
  }



  ngOnInit(): void {

    this.authSocialService.authState.subscribe(
      data => {
        this.socialUserLogged = data;
        this.isLoggedIn = (this.socialUserLogged != null && this.tokenStorageService.getToken() != null);
        this.firstName = this.socialUserLogged.firstName;
        this.photo = this.socialUserLogged.photoUrl;
        this.isLoggedIn = true;
        this.showUserBoard = true;
        this.roles = this.tokenStorageService.getUser().roles;

      }
    );

    if (this.socialUserLogged == null){

      this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {

      this.tokenStorageService.getToken();
      const user = this.tokenStorageService.getUser();
      this.firstName = user.firstName;
      this.roles = user.roles;
      this.showUserBoard = true;
      this.roles = this.tokenStorageService.getUser().roles;

      if (this.roles.includes("ROLE_ADMIN")){
        this.showAdminBoard = true;
      }
      if (this.roles.includes("ROLE_MODERATOR")){
        this.showModeratorBoard = true;
      }
    }
  }
}



      // this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      // this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR')


  logout(): void {
    this.tokenStorageService.signOut();
    this.isLoggedIn = false;
    window.location.reload();
  }


}

