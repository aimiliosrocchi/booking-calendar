import {Component, Input, OnInit} from '@angular/core';
import { UserService } from '../_services/user.service';
import {MovieService} from "../_services/movie.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit {
  content?: string;
  movies?: any;



  constructor(private router: Router, private userService: UserService, private MovieService: MovieService) { }

  ngOnInit(): void {

    this.userService.getUserBoard().subscribe(
      data => {
        this.MovieService.getMovies().subscribe(
          data =>{
            this.movies = data;
          },
          err => {
            this.movies = JSON.parse(err.error).message;
          }
        );
        },
      err => {
        if(err.message){
          this.content = err.message;
          alert("You must login in order to view the available movies");
          this.router.navigate(['/login']);

        }else{
          this.content = JSON.parse(err.error).message;
          alert(this.content);
          this.router.navigate(['/login']);
        }
      }
    );
  }
}
