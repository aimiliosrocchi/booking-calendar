import { BookingService } from '../_services/booking.service';
import { Booking } from '../booking';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HttpClient } from '@angular/common/http';
import {MovieService} from "../_services/movie.service";
import {Movie} from "../movie";
import {UserService} from "../_services/user.service";
@Component({
  selector: 'app-book-ticket',
  templateUrl: './book-ticket.component.html',
  styleUrls: ['./book-ticket.component.css']
})
export class BookTicketComponent implements OnInit {

  booking: Booking = new Booking();
  submitted = false;
  titleArray: any;
  content: any;

  constructor(private userService: UserService,public http:HttpClient, private router: Router, private movieService: MovieService) {}

  var_firstName="";
  var_lastName="";
  var_emailId="";
  var_phone="";
  var_address="";
  var_movie="";
  var_date="";



  onSubmit(){

    var booking = {

      "id":this.var_firstName,
      "firstName":this.var_firstName,
      "lastName":this.var_lastName,
      "emailId":this.var_emailId,
      "phone":this.var_phone,
      "address":this.var_address,
      "movie":this.var_movie,
      "date":this.var_date,
    }


    this.http.post<any>("http://localhost:8080/api/movies/bookings", booking).subscribe(data=>{

      alert("Movie booked successfully");
      this.gotoList();
    });
  }

  ngOnInit() {
    this.userService.getUserBoard().subscribe(
      data =>{
      this.movieService.getMovies().subscribe(
        data => {
          this.titleArray = data;
        },
        err => {
          this.titleArray = JSON.parse(err.error).message;
        }
      );
    }, err => {
        if(err.message){
          this.content = err.message;
          alert("You must Login / Signup to book your ticket");
          this.router.navigate(['/login']);

        }else{
          this.content = JSON.parse(err.error).message;
          alert(this.content);
          this.router.navigate(['/login']);
        }
    }
  );
}

  newBooking(): void {
    this.submitted = false;
    this.booking = new Booking();
  }

  gotoList() {
    this.router.navigate(['/bookings']);
  }
}
