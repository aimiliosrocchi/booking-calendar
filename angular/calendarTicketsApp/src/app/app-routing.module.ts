import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import {CalendarComponent} from "../calendar/calendar.component";
import {BookTicketComponent} from "./book-ticket/book-ticket.component";
import {BookingListComponent} from "./booking-list/booking-list.component";
import {TDetailsComponent} from "./t-details/t-details.component";
import {UpdateBookingComponent} from "./update-booking/update-booking.component";
import {AboutUsComponent} from "./about-us/about-us.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {path: 'events', component: CalendarComponent},
  { path: 'bookings', component: BookingListComponent },
  { path: 'add-movie', component: BookTicketComponent },
  { path: 'update/:id', component: UpdateBookingComponent },
  { path: 'details/:id', component: TDetailsComponent },
  { path: 'about', component: AboutUsComponent },






];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{}
