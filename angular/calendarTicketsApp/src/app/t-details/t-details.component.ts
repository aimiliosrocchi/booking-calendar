import { Booking } from '../booking';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { BookingService } from '../_services/booking.service';
import { BookingListComponent } from '../booking-list/booking-list.component';
import { Router, ActivatedRoute } from '@angular/router';
import {UserService} from "../_services/user.service";
import {QrCodeService} from "../_services/qr-code.service";

import jsPDF from 'jspdf';

import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfMake from 'html-to-pdfmake'

@Component({
  selector: 'app-t-details',
  templateUrl: './t-details.component.html',
  styleUrls: ['./t-details.component.css']
})
export class TDetailsComponent implements OnInit {

  @ViewChild('pdfTable') pdfTable: ElementRef | undefined;
  id!: string;
  booking: Booking | undefined;
  content: any;
  qrCode: any;
  isImageLoading: any;


  constructor(private route: ActivatedRoute, private router: Router,
              private bookingService: BookingService, private userService: UserService, private qrCodeService: QrCodeService) {
  }

  ngOnInit() {
    this.userService.getUserBoard().subscribe(
      data => {
        this.booking = new Booking();

        this.id = this.route.snapshot.params['id'];

        this.bookingService.getBooking(this.id)
          .subscribe(data => {
            this.booking = data;
          }, error => console.log(error));
      },
      err => {
        this.content = JSON.parse(err.error).message;
        alert(this.content);
        this.router.navigate(['/']);
      }
    );


    function makeid(length: number) {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
          charactersLength));
      }
      return result;
    }

    this.qrCodeService.getQrCode(makeid(5)).subscribe(data => {
        this.isImageLoading = true;
        this.qrCode =  this.createImageFromBlob(data);
        this.isImageLoading = false;
      },
      error => console.log(error));
      this.isImageLoading = false;


  }
    list()
    {
      this.router.navigate(['bookings']);
    }
  imageToShow: any;


  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  public downloadAsPDF() {
    const doc = new jsPDF();

    const pdfTable = this.pdfTable.nativeElement;

    var html = htmlToPdfMake(pdfTable.innerHTML);

    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).download();

  }
}


