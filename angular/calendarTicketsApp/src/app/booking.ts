//model booking class

export class Booking {
  id: string | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  emailId: string | undefined;
  phone: string | undefined;
  address: string | undefined;
  movie: string | undefined;
  date: string | undefined;

}
