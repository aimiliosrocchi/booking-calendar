import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import {MovieService} from "../_services/movie.service";
import {Movie} from "../movie";
import {NgForm} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {
  content?: string;
  movies?: any;
  event?: any;
  editMovie?: Movie | null;
  deleteMovie?: Movie | null;
  file?: any;
  imageUrl?: any;

  constructor(private router: Router,private userService: UserService, private movieService: MovieService) { }

  ngOnInit(): void {

    this.userService.getAdminBoard().subscribe(
      data => {
         this.movieService.getMovies().subscribe(
          data => {
            this.movies = data;
          },
          err => {
            this.movies = JSON.parse(err.error).message;
          }
        );
      },
      err => {
        if(err.message){
          this.content = err.message;
          alert("Only Administrator can access this page. Navigate to Home Page..");
          this.router.navigate(['/']);

        }else{
          this.content = JSON.parse(err.error).message;
          alert(this.content);
          this.router.navigate(['/']);
        }

      }
    );
  }

  onOpenModal(movie: Movie | null, mode: string): void{
    const container = document.getElementById("main-container");
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute("data-toggle","modal");
    if(mode === 'add'){
      button.setAttribute("data-target","#addMovieModal");
    }
    if(mode === 'edit'){
      this.editMovie = movie;
      button.setAttribute("data-target","#updateMovieModal");
    } if(mode === 'delete'){
      this.deleteMovie = movie;
      button.setAttribute("data-target","#deleteMovieModal");
    }
    if (container != null) {
      container.appendChild(button);
      button.click();
    }
  }

  onAddMovie(addForm: NgForm): void{
    const clearModule = document.getElementById("addMovieForm");
    if(clearModule !=null){
      clearModule.click();
    }
    this.movieService.addMovie(addForm.value).subscribe(
      (response: Movie) => {
        this.movieService.getMovies().subscribe( data => {
            this.movies = data;
            addForm.reset();
          },
          err => {
            this.movies = JSON.parse(err.error).message;
            addForm.reset();

          });

      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
        addForm.reset();

      }
    )
  }

  onEditMovie(movie: Movie): void{
    this.movieService.updateMovie(movie).subscribe(
      (response: Movie) => {
        this.movieService.getMovies().subscribe( data => {
            this.movies = data;
          },
          err => {
            this.movies = JSON.parse(err.error).message;
          });
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
      }
    )
  }

  onDeleteMovie(movieId: number): void{
    const clearModule = document.getElementById("deleteMovieForm");
    if(clearModule !=null){
      clearModule.click();
    }
    this.movieService.deleteMovie(movieId).subscribe(
      (response: void) => {
        this.movieService.getMovies().subscribe( data => {
            this.movies = data;
          },
          err => {
            this.movies = JSON.parse(err.error).message;
          });
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
      }
    )
  }


  onSelectFile({event}: { event: any }){
    if(event.target.files.length > 0){
      const file = event.target.files[0];

      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) == null){
        alert("Only images are supported.");
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) =>{
        this.imageUrl =reader.result;
      }
    }

  }
}
