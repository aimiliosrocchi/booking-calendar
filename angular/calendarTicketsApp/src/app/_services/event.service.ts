import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Event} from "../event";

const API_URL = 'http://localhost:8080/api/events/';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  constructor(private http: HttpClient) { }

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(API_URL + 'all');
  }

  addEvent(event: Event): Observable<Event> {
    return this.http.post<Event>(API_URL + 'add', event);
  }

  updateEvent(event: Event): Observable<Event> {
    return this.http.put<Event>(API_URL + 'update', event);
  }

  deleteEvent(eventId: number): Observable<void> {
    return this.http.delete<void>(API_URL + `delete/${eventId}`);
  }
}
