import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {TokenDto} from "../token-dto";

const AUTH_API = 'http://localhost:8080/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  public google(tokenDto: TokenDto): Observable<TokenDto>{
    return this.http.post<TokenDto>(AUTH_API  + 'google', tokenDto, httpOptions);
  }

  public facebook(tokenDto: TokenDto): Observable<TokenDto>{
    return this.http.post<TokenDto>(AUTH_API  + 'facebook', tokenDto, httpOptions);
  }


  login(email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      email,
      password
    }, httpOptions);
  }

  register(username: string, email: string, password: string, firstName: string, lastName: string): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      username,
      email,
      password,
      firstName,
      lastName
    }, httpOptions);
  }
}
