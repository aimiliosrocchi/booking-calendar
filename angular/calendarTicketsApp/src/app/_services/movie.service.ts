import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Movie} from "../movie";

const API_URL = 'http://localhost:8080/api/movies/';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  constructor(private http: HttpClient) { }

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(API_URL + 'all');
  }

  addMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(API_URL + 'add', movie);
  }

  updateMovie(movie: Movie): Observable<Movie> {
    return this.http.put<Movie>(API_URL + 'update', movie);
  }

  deleteMovie(movieId: number): Observable<void> {
    return this.http.delete<void>(API_URL + `delete/${movieId}`);
  }
}
