import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class QrCodeService {
  private API_URL = 'http://localhost:8080/api/qrcode';

  constructor(private http: HttpClient) { }

  getQrCode(barcode: string): Observable<Blob> {
    return this.http.get(`${this.API_URL}/${barcode}`, {responseType: "blob"});
  }
}
