import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import {SocialAuthService} from "angularx-social-login";
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import {MovieService} from "../_services/movie.service";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'] ,
providers: [NgbCarouselConfig]
})
export class HomeComponent implements OnInit {
  content?: string;
  movies?: any;
title = 'ng-carousel-movies';


  constructor(private userService: UserService, private oauthService: SocialAuthService, private MovieService: MovieService, config: NgbCarouselConfig) {
  config.interval = 2000;
    config.keyboard = true;
    config.pauseOnHover = true;
    config.animation = false;
  }

  ngOnInit(): void {

    this.MovieService.getMovies().subscribe(
      data =>{
        this.movies = data;
      },
      err => {
        this.movies = JSON.parse(err.error).message;
      }
    );
    this.userService.getPublicContent().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }
}
