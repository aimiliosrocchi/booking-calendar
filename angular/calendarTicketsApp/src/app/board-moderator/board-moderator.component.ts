import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import {Movie} from "../movie";
import {MovieService} from "../_services/movie.service";
import {NgForm} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css']
})
export class BoardModeratorComponent implements OnInit {
  content?: string;
  movies?: any;
  editMovie?: Movie | null;

  constructor(private router: Router, private userService: UserService,  private movieService: MovieService) { }

  ngOnInit(): void {
    this.userService.getModeratorBoard().subscribe(
      data => {
        this.movieService.getMovies().subscribe(
          data => {
            this.movies = data;
          },
          err => {
            this.movies = JSON.parse(err.error).message;
          }
        );
      },
      err => {
        if(err.message){
          this.content = err.message;
          alert("Only Moderator and Administrator can access this page. Navigate to Home Page..");
          this.router.navigate(['/']);

        }else{
          this.content = JSON.parse(err.error).message;
          alert(this.content);
          this.router.navigate(['/']);
        }
      }
    );
  }


  onOpenModal(movie: Movie | null, mode: string): void{
    const container = document.getElementById("main-container");
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute("data-toggle","modal");
    if(mode === 'add'){
      button.setAttribute("data-target","#addMovieModal");
    }
    if(mode === 'edit'){
      this.editMovie = movie;
      button.setAttribute("data-target","#updateMovieModal");
    }
    if (container != null) {
      container.appendChild(button);
      button.click();
    }
  }

  onAddMovie(addForm: NgForm): void{
    const clearModule = document.getElementById("addMovieForm");
    if(clearModule !=null){
      clearModule.click();
    }
    this.movieService.addMovie(addForm.value).subscribe(
      (response: Movie) => {
        this.movieService.getMovies().subscribe( data => {
            this.movies = data;
            addForm.reset();
          },
          err => {
            this.movies = JSON.parse(err.error).message;
            addForm.reset();

          });

      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
        addForm.reset();

      }
    )
  }

  onEditMovie(movie: Movie): void{
    this.movieService.updateMovie(movie).subscribe(
      (response: Movie) => {
        this.movieService.getMovies().subscribe( data => {
            this.movies = data;
          },
          err => {
            this.movies = JSON.parse(err.error).message;
          });
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
      }
    )
  }
}
