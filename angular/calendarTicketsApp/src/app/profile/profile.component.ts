import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import {SocialAuthService, SocialUser} from "angularx-social-login";
import {UserService} from "../_services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser: any;
  isSocial = false;
  content: any;

  constructor(private token: TokenStorageService, private authSocialService: SocialAuthService, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.userService.getUserBoard().subscribe(
      data =>{
        this.authSocialService.authState.subscribe(
          data => {
            this.currentUser = data;
            this.isSocial = true;

          }
        );

        if(!this.isSocial) {
          this.currentUser = this.token.getUser();
        }
      }, err => {
        if(err.message){
          this.content = err.message;
          alert("You must Login / Register in order to view your profile");
          this.router.navigate(['/login']);

        }else{
          this.content = JSON.parse(err.error).message;
          alert(this.content);
          this.router.navigate(['/login']);
        }
      }
    );

  }
}
