import { Component, OnInit } from '@angular/core';
import { Booking } from '../booking';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../_services/booking.service';
import {UserService} from "../_services/user.service";

@Component({
  selector: 'app-update-booking',
  templateUrl: './update-booking.component.html',
  styleUrls: ['./update-booking.component.css']
})
export class UpdateBookingComponent implements OnInit {




  id!: string;
  booking: Booking | undefined;
  content: any;

  constructor(private route: ActivatedRoute,private router: Router,
              private bookingService: BookingService, private userService: UserService) { }

  ngOnInit() {
    this.userService.getUserBoard().subscribe(
      data =>{
    this.booking = new Booking();

    this.id = this.route.snapshot.params['id'];

    this.bookingService.getBooking(this.id)
      .subscribe(data => {
        console.log(data)
        this.booking = data;
      }, error => console.log(error));
   },
      err => {
        this.content = JSON.parse(err.error).message;
        alert(this.content);
        this.router.navigate(['/']);
      }
    );
  }

  updateBooking() {
    this.bookingService.updateBooking(this.id, this.booking)
      .subscribe(data => {
        console.log(data);
        this.booking = new Booking();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateBooking();
  }

  gotoList() {
    this.router.navigate(['/bookings']);
  }

}
