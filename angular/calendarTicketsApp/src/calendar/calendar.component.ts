import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef, OnInit,
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import { Subject } from 'rxjs';
// @ts-ignore
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import {Router} from "@angular/router";
import {EventService} from "../app/_services/event.service";
import {UserService} from "../app/_services/user.service";

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'mwl-calendar-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `


      pre {
        background-color: #f5f5f5;
        padding: 15px;
      }
    `,
  ],
  templateUrl: 'calendar.template.html',
  styleUrls: ['calendar.component.css'],


})
export class CalendarComponent  implements OnInit {
  events?: any;
  content?: string;
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any> | undefined;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  refresh = new Subject<void>();

  activeDayIsOpen: boolean = true;

  constructor(private modal: NgbModal, private router: Router, private eventService: EventService, private userService: UserService) {}

    ngOnInit(): void {
      this.userService.getAdminBoard().subscribe(
        data => {
          this.eventService.getEvents().subscribe(
            data => {
              this.events = data;
            },
            err => {
              this.events = JSON.parse(err.error).message;
            }
          );
        },
        err => {
          if(err.message){
            this.content = err.message;
            alert("Only Administrator can access this page. Navigate to Home Page..");
            this.router.navigate(['/home']);

          }else{
            this.content = JSON.parse(err.error).message;
            alert(this.content);
            this.router.navigate(['/home']);
          }
        }
      );
    }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  // eventTimesChanged({
  //                     event,
  //                     newStart,
  //                     newEnd,
  //                   }: CalendarEventTimesChangedEvent): void {
  //   this.events = this.events.map((iEvent) => {
  //     if (iEvent === event) {
  //       return {
  //         ...event,
  //         start: newStart,
  //         end: newEnd,
  //       };
  //     }
  //     return iEvent;
  //   });
  //   this.handleEvent('Dropped or resized', event);
  // }
  //
  // handleEvent(action: string, event: CalendarEvent): void {
  //   this.eventService = { event, action };
  //   this.modal.open(this.modalContent, { size: 'lg' });
  // }

  addNewEvent(): void {

  }

  deleteEvent(eventToDelete: CalendarEvent) {
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
}
