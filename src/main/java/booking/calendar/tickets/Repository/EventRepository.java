package booking.calendar.tickets.Repository;

import booking.calendar.tickets.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    void deleteEventById(Long eventId);

    Optional<Event> findEventById(Long eventId);
}
