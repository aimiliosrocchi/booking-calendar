package booking.calendar.tickets.security;

import booking.calendar.tickets.model.User;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;


import java.util.List;
import java.util.stream.Collectors;

public class UserPrincipalFactory {

    public static UserPrincipal build(User user){
        List<GrantedAuthority> authorities =
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());

        return new UserPrincipal(user.getEmail(), user.getPassword(), authorities);
    }


}
