package booking.calendar.tickets.security.services;

import booking.calendar.tickets.Repository.RoleRepository;
import booking.calendar.tickets.model.ERole;
import booking.calendar.tickets.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public Optional<Role> getRoleByName(ERole name){
        return roleRepository.findByName(name);
    }

//    public boolean existsRoleName(ERole name){
//        return roleRepository.existsByRoleName(name);
//    }

    public void save(Role role){
        roleRepository.save(role);
    }
}
