package booking.calendar.tickets.security.services;

import booking.calendar.tickets.Repository.EventRepository;
import booking.calendar.tickets.exception.UserNotFoundException;
import booking.calendar.tickets.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EventService {
    @Autowired
    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Event addEvent(Event event) {
        return eventRepository.save(event);
    }

    public List<Event> findAllEvents() {
        return eventRepository.findAll();
    }

    public Event updateEvent(Event event) {
        return eventRepository.save(event);
    }

    public void deleteEvent(Long eventId) {
        eventRepository.deleteEventById(eventId);
    }

    public Event findEventById(Long eventId) {
        return eventRepository.findEventById(eventId)
                .orElseThrow(() -> new UserNotFoundException("Event by id" + eventId + "was not found"));
    }
}