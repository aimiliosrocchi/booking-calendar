package booking.calendar.tickets.security.services;

import booking.calendar.tickets.Repository.MovieRepository;
import booking.calendar.tickets.exception.UserNotFoundException;
import booking.calendar.tickets.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MovieService {

    @Autowired
    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public Movie addMovie(Movie movie){
        return movieRepository.save(movie);
    }

    public List<Movie> findAllMovies(){
        return movieRepository.findAll();
    }

    public Movie updateMovie(Movie movie){
        return movieRepository.save(movie);
    }

    public void deleteMovie(Long id){
        movieRepository.deleteMovieById(id);
    }

    public Movie findMovieById(Long id){
        return movieRepository.findMovieById(id)
                .orElseThrow(()-> new UserNotFoundException("Movie by id" + id + "was not found"));
    }
}
