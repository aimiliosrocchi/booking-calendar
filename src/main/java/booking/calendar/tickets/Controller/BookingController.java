package booking.calendar.tickets.Controller;

import booking.calendar.tickets.model.Booking;
import booking.calendar.tickets.security.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/movies/")
public class BookingController {



    @Autowired
    private BookingService bookingService;

    @CrossOrigin
    @RequestMapping("/bookings")
    public java.util.List<Booking> getAllBookings() {

        return bookingService.getAllBookings();

    }

    @CrossOrigin
    @RequestMapping("/bookings/{id}")
    public Optional<Booking> getBookings(@PathVariable String id) {

        return bookingService.getBookings(id);
    }

    @CrossOrigin
    @RequestMapping(method=RequestMethod.POST, value= "/bookings")
    public void addBookings(@RequestBody Booking newMovie) {
        bookingService.addBookings(newMovie);
    }



    @CrossOrigin
    @RequestMapping(method=RequestMethod.DELETE, value= "/bookings/{id}")
    public void deleteBookings(@PathVariable String id) {
        bookingService.deleteBookings(id);
    }


    @CrossOrigin
    @RequestMapping(method=RequestMethod.PUT,value="/bookings/{id}")
    public void updateBookings(@RequestBody Booking updatedMovie, @PathVariable String id) {
        bookingService.updateBookings(updatedMovie,id);
    }





}