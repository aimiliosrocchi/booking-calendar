package booking.calendar.tickets.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "events", uniqueConstraints = {
        @UniqueConstraint(columnNames = "title")
})
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, length = 50)
    private String title;


    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "end_date", nullable = true)
    private Date endDate;

    @Column(name = "is_full_day", nullable = true)
    private boolean isFullDay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isFullDay() {
        return isFullDay;
    }

    public void setFullDay(boolean fullDay) {
        isFullDay = fullDay;
    }

    public Event(){}

    public Event(String title, Date startDate, Date endDate, boolean isFullDay){
        super();
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isFullDay = isFullDay;
    }


}
