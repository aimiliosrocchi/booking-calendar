package booking.calendar.tickets.model;

import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Entity
@Table(name = "movies", uniqueConstraints = {
        @UniqueConstraint(columnNames = "title")
})
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 40)
    private String title;

    @NotBlank
    @Size(max = 200)
    private String description;

    @NotBlank
    @Size(max = 200)
    @URL
    private String imageUrl;

    @Size(max = 2000)
    private String summary;


    public void setId(Long id) {
        this.id = id;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Long getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    public Movie() {}

    public Movie(String imageUrl, String title, String description, String summary){
        super();
        this.imageUrl = imageUrl;
        this.title = title;
        this.description = description;
        this.summary = summary;
    }


}
