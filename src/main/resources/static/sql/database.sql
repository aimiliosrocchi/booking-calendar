-- step 1: -- open mysql client cmd
-- password: test
-- step 2: run this on terminal
DROP DATABASE IF EXISTS webAppProject;
-- step 3: run this on terminal
CREATE DATABASE webAppProject;


-- step 4: open DBeaver Client and connect to database webAppProject
-- username: root, password: test
-- from toolbar select New SQL editor
-- copy all the below script and paste it into script on DBeaver
-- select execute sql script or (ALT+X)
DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `id` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `movie` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `booking` WRITE;

UNLOCK TABLES;

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` datetime(6) DEFAULT NULL,
  `is_full_day` bit(1) DEFAULT NULL,
  `start_date` datetime(6) NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKs28f48qss2cnh9fowua47xjss` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


LOCK TABLES `events` WRITE;
INSERT INTO `events` VALUES (1,'2022-04-26 02:00:00.000000','\0','2022-04-26 02:00:00.000000','test');
UNLOCK TABLES;


DROP TABLE IF EXISTS `movies`;

CREATE TABLE `movies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) DEFAULT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `summary` varchar(2000) DEFAULT NULL,
  `title` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKgovm2dombrdnujupo3o7hvtep` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


LOCK TABLES `movies` WRITE;
INSERT INTO `movies` VALUES (1,'“Nobody is perfect. Everyone has their own little idiosyncrasies. Some people call those imperfections, but no, that’s the good stuff.”','http://localhost:8080/images/goodwillhunting.jpg','Winning an Oscar for the Best Screenplay in 1998, Good Will Hunting features at the top of our list of educational movies which is written by the duo, Ben Affleck and Matt Damon. The mathematical prodigy, Will, is brilliant in solving complicated numerical problems but finds himself struggling with the complex equation of them all, i.e. life. He is approached by a mathematics professor to work on a problem but noticing that he is a law-breaker and rebel, he is instead sent to a psychology teacher, Sean Maguire, who helps him face his inner demons and understand what life is really about. ','Good Will Hunting'),(2,'“Never study to be successful, study for self-efficiency. Don’t run behind success. Follow behind excellence, success will come all way behind you.”','http://localhost:8080/images/idiots.jpg','If you haven’t really heard of Rancho’s lessons on career and life, then you have surely missed out on real-world education! A coming-of-age comedy-drama, this blockbuster, and critically appreciated film depict the life of engineering students, from the pacing race to be at the top of academics to tackling the ragging of seniors. Amongst the best educational movies of all the time, it encompasses the story of three friends, Rancho, Raju and Farhan, and their college days and takes you on a rollercoaster of learning the ways of the real world, following your inner calling rather than marks, and promoting the ideology of thinking out-of-the-box which can take you places afar!','3 Idiots'),(3,'“Knowledge counts but common sense matters.”','http://localhost:8080/images/dangerous.jpg','This 1995 American Drama is another much-deserved mention in our compilation of educational movies. Based on the true story of LouAnne Johnson, it depicts the endeavours of a retired U.S. Marine turned teacher who was placed in Carlmont High School in a class of African-American and Latino teenagers. Having been called “white bread”, she finds her students engrossed in gang warfare, drug pushing and reluctant to be involved in classroom learning in any manner. The film elucidates her quest to implement engaging learning methods using music and extracurricular activities. It emphasizes on how a teacher can bring students to the right path and help them transform into better individuals.','Dangerous Minds'),(4,'“I service society by rocking, ok? I’m out there on the front lines liberating people with my music!”','http://localhost:8080/images/schoolOfRock.jpg','An eccentric and passionate musician, Dewey Finn finds himself kicked out of his band with which he was going to appear for the Battle of the Bands competition. He does not lose hope and instead, impersonates his best friend to fill the position of a substitute teacher at a prep school in order to earn some money for his house rent. As he begins at his job, his erratic behaviour and no interest in teaching students confuse them but one day, he sees his class during their music session and decides to build his own band for the competition. As one of the funniest yet inspirational and educational movies, the School of Rock is a musical masterpiece that is going to change the way you perceived music especially, the genre of rock and will teach you about teamwork, persistence, and perseverance!','The School of Rock'),(5,'“The problem isn’t the kids. It’s not even what they can achieve. The problem is what you expect them to achieve. You are setting the bar here. Why? Set it up here. They can make it.”','http://localhost:8080/images/ronclarkstory.jpg','Bringing the Friends fame Matthew Perry into a new light, The Ron Clark Story is about an idealistic teacher who moves to Manhattan and gets placed in a class filled with poverty-stricken students who have a reluctant behaviour towards education. As one of those educational movies based on a true story, it depicts the efforts of the renowned American educator Ron Clark who took the challenge of improving his student’s test scores and getting them to qualify for the grade. He ends doing a lot more with his interactive teaching and different learning methods, as he reinstills courage and hope into the students, and helping them pursue what they are passionate about!','The Ron Clark Story');
UNLOCK TABLES;


DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_MODERATOR'),(3,'ROLE_USER');
UNLOCK TABLES;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;


LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (17,'admin@gmail.com','Administrator','Adminakis','$2a$10$d8w1pbtgcw9JFEn0AaQTB.5iElyO4fhGQGsSs/X2AHLRX.NvlxuFO','adminious'),(18,'mod@gmail.com','Moderator','Moderakis','$2a$10$iSVLfBdRQEtsdgef6JUne.WJQtkcf3TEowBLEMFwlvMrC6htZ5VdK','moderious'),(19,'user@gmail.com','User','Userakis','$2a$10$SI8tvzKXStnJYzF/rdzu9ePw6CA3DTRWxvw/ZPz/4Vg51cTWSHkoK','Userious');
UNLOCK TABLES;

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`),
  CONSTRAINT `FKh8ciramu9cc9q3qcqiv4ue8a6` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FKhfh9dx7w3ubf1co1vdev94g3f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


LOCK TABLES `user_roles` WRITE;
INSERT INTO `user_roles` VALUES (17,1),(18,2),(19,3);
UNLOCK TABLES;



