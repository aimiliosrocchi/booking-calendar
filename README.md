Project Info:

Booking Application using Spring Boot and Angular Frameworks

Role Management System, Content Management for Admins (movies) and Booking System

Login / Register Users using JWT Authentication

Social Login Provided for Facebook and Google using Oauth 2.0 Authentication

REST Application



Requirements
1) Node
2) Angular CLI 13.1.0
3) JDK 1.8 or 11

Backend Spring Boot Application runs on localhost:8080

FrontEnd Angular Application runs on localhost:4200

MariaDB Database Server runs on localhost:3306


